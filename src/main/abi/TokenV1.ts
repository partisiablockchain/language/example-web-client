/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class TokenV1 {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;

  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeTokenState(_input: AbiInput): TokenState {
    const name: string = _input.readString();
    const decimals: number = _input.readU8();
    const symbol: string = _input.readString();
    const owner: BlockchainAddress = _input.readAddress();
    const totalSupply: BN = _input.readUnsignedBigInteger(16);
    const balances_mapLength = _input.readI32();
    const balances: Map<BlockchainAddress, BN> = new Map();
    for (let balances_i = 0; balances_i < balances_mapLength; balances_i++) {
      const balances_key: BlockchainAddress = _input.readAddress();
      const balances_value: BN = _input.readUnsignedBigInteger(16);
      balances.set(balances_key, balances_value);
    }
    const allowed_mapLength = _input.readI32();
    const allowed: Map<BlockchainAddress, Map<BlockchainAddress, BN>> = new Map();
    for (let allowed_i = 0; allowed_i < allowed_mapLength; allowed_i++) {
      const allowed_key: BlockchainAddress = _input.readAddress();
      const allowed_value_mapLength = _input.readI32();
      const allowed_value: Map<BlockchainAddress, BN> = new Map();
      for (let allowed_value_i = 0; allowed_value_i < allowed_value_mapLength; allowed_value_i++) {
        const allowed_value_key: BlockchainAddress = _input.readAddress();
        const allowed_value_value: BN = _input.readUnsignedBigInteger(16);
        allowed_value.set(allowed_value_key, allowed_value_value);
      }
      allowed.set(allowed_key, allowed_value);
    }
    return { name, decimals, symbol, owner, totalSupply, balances, allowed };
  }
  public async getState(): Promise<TokenState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeTokenState(input);
  }
}
export interface TokenState {
  name: string;
  decimals: number;
  symbol: string;
  owner: BlockchainAddress;
  totalSupply: BN;
  balances: Map<BlockchainAddress, BN>;
  allowed: Map<BlockchainAddress, Map<BlockchainAddress, BN>>;
}

export interface Transfer {
  to: BlockchainAddress;
  amount: BN;
}
function serializeTransfer(_out: AbiOutput, _value: Transfer): void {
  const { to, amount } = _value;
  _out.writeAddress(to);
  _out.writeUnsignedBigInteger(amount, 16);
}

export function initialize(
  name: string,
  symbol: string,
  decimals: number,
  totalSupply: BN
): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeString(name);
    _out.writeString(symbol);
    _out.writeU8(decimals);
    _out.writeUnsignedBigInteger(totalSupply, 16);
  });
}

export function transfer(to: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function bulkTransfer(transfers: Transfer[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
    _out.writeI32(transfers.length);
    for (const transfers_vec of transfers) {
      serializeTransfer(_out, transfers_vec);
    }
  });
}

export function transferFrom(from: BlockchainAddress, to: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("03", "hex"));
    _out.writeAddress(from);
    _out.writeAddress(to);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function bulkTransferFrom(from: BlockchainAddress, transfers: Transfer[]): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("04", "hex"));
    _out.writeAddress(from);
    _out.writeI32(transfers.length);
    for (const transfers_vec of transfers) {
      serializeTransfer(_out, transfers_vec);
    }
  });
}

export function approve(spender: BlockchainAddress, amount: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("05", "hex"));
    _out.writeAddress(spender);
    _out.writeUnsignedBigInteger(amount, 16);
  });
}

export function approveRelative(spender: BlockchainAddress, delta: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("07", "hex"));
    _out.writeAddress(spender);
    _out.writeSignedBigInteger(delta, 16);
  });
}

export function deserializeState(state: StateWithClient): TokenState;
export function deserializeState(bytes: Buffer): TokenState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): TokenState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): TokenState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new TokenV1(client, address).deserializeTokenState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new TokenV1(state.client, state.address).deserializeTokenState(input);
  }
}
