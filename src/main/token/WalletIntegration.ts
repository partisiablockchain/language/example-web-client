/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  resetAccount,
  setAccount,
  getContractAddress,
  isConnected,
  getContractLastBalanceKey,
  setContractLastBalanceKey,
  getTokenApi,
} from "./AppState";
import { connectMetaMask } from "../shared/MetaMaskSignatureProvider";
import { connectMpcWallet } from "../shared/MpcWalletSignatureProvider";
import { connectLedger, validateLedgerConnection } from "../shared/LedgerSignatureProvider";
import { connectPrivateKey } from "../shared/PrivateKeySignatureProvider";
import { CryptoUtils } from "../client/CryptoUtils";
import { SenderAuthentication } from "@partisiablockchain/blockchain-api-transaction-client";

/**
 * Function for connecting to the MPC wallet and setting the connected wallet in the app state.
 */
export const connectMetaMaskWalletClick = () => {
  handleWalletConnect(connectMetaMask());
};

/**
 * Function for connecting to the MPC wallet and setting the connected wallet in the app state.
 */
export const connectLedgerWalletClick = () => {
  handleWalletConnect(connectLedger());
  setVisibility("#connection-link-ledger-validate", true);
};

/**
 * Shows the address on the Ledger.
 */
export const validateLedgerConnectionClick = () => {
  validateLedgerConnection();
};

/**
 * Function for connecting to the MPC wallet and setting the connected wallet in the app state.
 */
export const connectMpcWalletClick = () => {
  // Call Partisia SDK to initiate connection
  handleWalletConnect(connectMpcWallet());
};

/**
 * Connect to the blockchain using a private key. Reads the private key from the form.
 */
export const connectPrivateKeyWalletClick = () => {
  const privateKey = <HTMLInputElement>document.querySelector("#private-key-value");
  const keyPair = CryptoUtils.privateKeyToKeypair(privateKey.value);
  const sender = CryptoUtils.keyPairToAccountAddress(keyPair);
  handleWalletConnect(connectPrivateKey(sender, keyPair));
};

/**
 * Common code for handling a generic wallet connection.
 * @param connect the wallet connection. Can be Mpc Wallet, Metamask, or using a private key.
 */
const handleWalletConnect = (connect: Promise<SenderAuthentication>) => {
  // Clean up state
  resetAccount();
  setConnectionStatus("Connecting...");
  connect
    .then((userAccount) => {
      setAccount(userAccount);

      // Fix UI
      setConnectionStatus("Logged in: ", userAccount.getAddress());
      setVisibility("#wallet-connect", false);
      setVisibility("#metamask-connect", false);
      setVisibility("#ledger-connect", false);
      setVisibility("#private-key-connect", false);
      setVisibility("#wallet-disconnect", true);
      updateInteractionVisibility();
    })
    .catch((error) => {
      console.error(error);
      if ("message" in error) {
        setConnectionStatus(error.message);
      } else {
        setConnectionStatus("An error occurred trying to connect wallet: " + error);
      }
    });
};

/**
 * Reset state to disconnect current user.
 */
export const disconnectWalletClick = () => {
  resetAccount();
  setConnectionStatus("Disconnected account");
  setVisibility("#wallet-connect", true);
  setVisibility("#metamask-connect", true);
  setVisibility("#ledger-connect", true);
  setVisibility("#private-key-connect", true);
  setVisibility("#wallet-disconnect", false);
  setVisibility("#connection-link-ledger-validate", false);
  updateInteractionVisibility();
};

const ALL_BALANCES_LIST = <HTMLElement>document.querySelector("#all-balances");

/**
 * Write some of the state to the UI.
 */
export const updateContractState = () => {
  const address = getContractAddress();
  if (address === undefined) {
    throw new Error("No address provided");
  }
  const tokenApi = getTokenApi();
  if (tokenApi === undefined) {
    throw new Error("Token API not setup");
  }

  const refreshLoader = <HTMLInputElement>document.querySelector("#refresh-loader");
  refreshLoader.classList.remove("hidden");

  if (tokenApi.basicState != undefined) {
    tokenApi.basicState(address).then((state) => {
      const stateHeader = <HTMLInputElement>document.querySelector("#state-header");
      const updateStateButton = <HTMLInputElement>document.querySelector("#update-state");
      stateHeader.classList.remove("hidden");
      updateStateButton.classList.remove("hidden");

      const name = <HTMLElement>document.querySelector("#name");
      name.innerHTML = `${state.name}`;

      const decimals = <HTMLElement>document.querySelector("#decimals");
      decimals.innerHTML = `${state.decimals}`;

      const symbol = <HTMLElement>document.querySelector("#symbol");
      symbol.innerHTML = `${state.symbol}`;

      const owner = <HTMLElement>document.querySelector("#owner");
      owner.innerHTML = `${state.owner.asString()}`;

      const totalSupply = <HTMLElement>document.querySelector("#total-supply");
      totalSupply.innerHTML = `${state.totalSupply}`;

      const contractState = <HTMLElement>document.querySelector("#contract-state");
      contractState.classList.remove("hidden");
      refreshLoader.classList.add("hidden");
    });
  }

  setContractLastBalanceKey(undefined);
  ALL_BALANCES_LIST.innerHTML = "";
  fetchAndDisplayMoreBalances();
};

function setConnectionStatus(status: string, address: string | undefined = undefined) {
  const statusText = <HTMLElement>document.querySelector("#connection-status");
  const statusLink = <HTMLAnchorElement>document.querySelector("#connection-link");

  statusText.innerHTML = status;
  statusLink.innerText = "";
  if (address != undefined) {
    statusLink.href = `https://browser.testnet.partisiablockchain.com/accounts/${address}`;
    statusLink.innerText = address;
  }
}

const setVisibility = (selector: string, visible: boolean) => {
  const element = <HTMLElement>document.querySelector(selector);
  if (visible) {
    element.classList.remove("hidden");
  } else {
    element.classList.add("hidden");
  }
};

export const updateInteractionVisibility = () => {
  const contractInteraction = <HTMLElement>document.querySelector("#contract-interaction");
  if (isConnected() && getContractAddress() !== undefined) {
    contractInteraction.classList.remove("hidden");
  } else {
    contractInteraction.classList.add("hidden");
  }
};

export const fetchAndDisplayMoreBalances = () => {
  const address = getContractAddress();
  if (address == undefined) {
    throw new Error("Address not set");
  }
  const tokenApi = getTokenApi();
  if (tokenApi === undefined) {
    throw new Error("Token API not setup");
  }
  if (tokenApi.tokenBalances == undefined) {
    return; // Contract doesn't support tokenBalances
  }

  const lastKey = getContractLastBalanceKey();
  tokenApi.tokenBalances(address, lastKey).then((balancesResult) => {
    balancesResult.balances.forEach((amount, tokenOwner) => {
      const balance = document.createElement("li");
      balance.innerHTML = `<span class="address">${tokenOwner.asString()}</span>: ${amount}`;
      ALL_BALANCES_LIST.appendChild(balance);
    });
    setContractLastBalanceKey(balancesResult.next_cursor);

    const loadMoreBtn = <Element>document.querySelector("#balances-load-more-btn");
    if (balancesResult.next_cursor === undefined) {
      loadMoreBtn.classList.add("hidden");
    } else {
      loadMoreBtn.classList.remove("hidden");
    }
  });
};
