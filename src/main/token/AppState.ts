/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ContractAbi, BlockchainAddress } from "@partisiablockchain/abi-client";
import { TokenContract } from "../shared/TokenContract";
import {
  BlockchainTransactionClient,
  ChainControllerApi,
  Configuration,
  SenderAuthentication,
} from "@partisiablockchain/blockchain-api-transaction-client";

export const CLIENT = new ChainControllerApi(
  new Configuration({ basePath: "https://node1.testnet.partisiablockchain.com" })
);

type TokenContractCreator = (
  client: ChainControllerApi,
  transactionClient: BlockchainTransactionClient | undefined
) => TokenContract;

let lastBalanceKey: BlockchainAddress | undefined;
let contractAddress: BlockchainAddress | undefined;
let currentAccount: SenderAuthentication | undefined;
let contractAbi: ContractAbi | undefined;
let tokenApi: TokenContract | undefined;

/*eslint @typescript-eslint/no-unused-vars: ["error", { "argsIgnorePattern": "^unused" }]*/

let tokenContractCreator: TokenContractCreator = (unusedClient, unusedContract) => {
  throw new Error("tokenContractCreator was not set");
};

export function setTokenContractType(creator: TokenContractCreator) {
  tokenContractCreator = creator;
}

export const setAccount = (account: SenderAuthentication | undefined) => {
  currentAccount = account;
  setTokenApi();
};

export const resetAccount = () => {
  currentAccount = undefined;
};

export const isConnected = () => {
  return currentAccount != null;
};

export const setContractAbi = (abi: ContractAbi) => {
  contractAbi = abi;
  setTokenApi();
};

export const getContractAbi = () => {
  return contractAbi;
};

const setTokenApi = () => {
  let transactionClient = undefined;
  if (currentAccount != undefined) {
    transactionClient = BlockchainTransactionClient.create(
      "https://node1.testnet.partisiablockchain.com",
      currentAccount
    );
  }
  tokenApi = tokenContractCreator(CLIENT, transactionClient);
};

export const getTokenApi = () => {
  return tokenApi;
};

export const getContractAddress = () => {
  return contractAddress;
};

export const setContractAddress = (address: BlockchainAddress) => {
  contractAddress = address;
  setTokenApi();
};

export const getContractLastBalanceKey = () => {
  return lastBalanceKey;
};

export const setContractLastBalanceKey = (address: BlockchainAddress | undefined) => {
  lastBalanceKey = address;
};
