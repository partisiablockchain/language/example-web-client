/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "@partisiablockchain/abi-client";
import BN from "bn.js";
import { transfer, TokenState, deserializeState } from "../../abi/TokenV1";
import {
  TokenContract,
  TokenContractBasicState,
  TokenBalancesResult,
} from "../../shared/TokenContract";
import {
  BlockchainTransactionClient,
  ChainControllerApi,
  SentTransaction,
} from "@partisiablockchain/blockchain-api-transaction-client";

export class TokenV1Contract implements TokenContract {
  private readonly transactionClient: BlockchainTransactionClient | undefined;
  private readonly client: ChainControllerApi;

  constructor(
    shardedClient: ChainControllerApi,
    transactionClient: BlockchainTransactionClient | undefined
  ) {
    this.transactionClient = transactionClient;
    this.client = shardedClient;
  }

  /**
   * Build and send transfer transaction.
   * @param to receiver of tokens
   * @param amount number of tokens to send
   */
  public transfer(
    contractAddress: BlockchainAddress,
    to: BlockchainAddress,
    amount: BN
  ): Promise<SentTransaction> {
    if (this.transactionClient === undefined) {
      throw new Error("No account logged in");
    }

    // First build the RPC buffer that is the payload of the transaction.
    const rpc = transfer(to, amount);
    // Then send the payload via the transaction API.
    // We are sending the transaction to the configured address of the token address, and use the
    // GasCost utility to estimate how much the transaction costs.
    return this.transactionClient.signAndSend({ address: contractAddress.asString(), rpc }, 10_000);
  }

  private getState(contractAddress: BlockchainAddress): Promise<TokenState> {
    return this.client.getContract({ address: contractAddress.asString() }).then((contract) => {
      if (contract == null) {
        throw new Error("Could not find data for contract");
      }

      // Reads the state of the contract
      if (contract.serializedContract != undefined) {
        const stateBuffer = Buffer.from(contract.serializedContract, "base64");
        return deserializeState(stateBuffer);
      } else throw new Error("Could not get the contract state.");
    });
  }

  public basicState(contractAddress: BlockchainAddress): Promise<TokenContractBasicState> {
    return this.getState(contractAddress);
  }

  /*eslint @typescript-eslint/no-unused-vars: ["error", { "argsIgnorePattern": "^unused" }]*/
  public tokenBalances(
    contractAddress: BlockchainAddress,
    unusedCursor: BlockchainAddress | undefined
  ): Promise<TokenBalancesResult> {
    return this.getState(contractAddress).then((state) => {
      return { balances: state.balances, next_cursor: undefined };
    });
  }
}
