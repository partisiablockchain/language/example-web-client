/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";
import {
  AvlTreeMap,
  BlockchainAddress,
  BlockchainStateClientImpl,
} from "@partisiablockchain/abi-client";
import { transfer, TokenV2 } from "../../abi/TokenV2";
import {
  TokenContract,
  TokenContractBasicState,
  TokenBalancesResult,
} from "../../shared/TokenContract";
import {
  BlockchainTransactionClient,
  ChainControllerApi,
  SentTransaction,
} from "@partisiablockchain/blockchain-api-transaction-client";

const CODEGEN_CLIENT = BlockchainStateClientImpl.create(
  "https://node1.testnet.partisiablockchain.com"
);

/**
 * API for the token contract.
 * This minimal implementation only allows for transferring tokens to a single address.
 *
 * The implementation uses the TransactionApi to send transactions, and ABI for the contract to be
 * able to build the RPC for the transfer transaction.
 */
export class TokenV2Contract implements TokenContract {
  private balanceAvlTree: AvlTreeMap<BlockchainAddress, BN> | undefined;
  private readonly transactionClient: BlockchainTransactionClient | undefined;
  private readonly client: ChainControllerApi;

  constructor(
    shardedClient: ChainControllerApi,
    transactionClient: BlockchainTransactionClient | undefined
  ) {
    this.transactionClient = transactionClient;
    this.client = shardedClient;
  }

  /**
   * Build and send transfer transaction.
   * @param contractAddress address of the contract
   * @param to receiver of tokens
   * @param amount number of tokens to send
   */
  public transfer(
    contractAddress: BlockchainAddress,
    to: BlockchainAddress,
    amount: BN
  ): Promise<SentTransaction> {
    if (this.transactionClient === undefined) {
      throw new Error("No account logged in");
    }

    // First build the RPC buffer that is the payload of the transaction.
    const rpc = transfer(to, amount);
    // Then send the payload via the transaction API.
    // We are sending the transaction to the configured address of the token address, and use the
    // GasCost utility to estimate how much the transaction costs.
    return this.transactionClient.signAndSend({ address: contractAddress.asString(), rpc }, 10_000);
  }

  public basicState(contractAddress: BlockchainAddress): Promise<TokenContractBasicState> {
    return new TokenV2(CODEGEN_CLIENT, contractAddress).getState().then((state) => {
      if (state == null) {
        throw new Error("Could not find data for contract");
      }

      this.balanceAvlTree = state.balances;
      return state;
    });
  }

  public async tokenBalances(
    contractAddress: BlockchainAddress,
    keyAddress: BlockchainAddress | undefined
  ): Promise<TokenBalancesResult> {
    if (this.balanceAvlTree === undefined) {
      await this.basicState(contractAddress);
    }
    const values = await this.balanceAvlTree?.getNextN(keyAddress, 10);
    if (values === undefined) {
      throw new Error("Could not fetch balances");
    }

    const balances: Map<BlockchainAddress, BN> = new Map();
    let lastKey: BlockchainAddress | undefined = undefined;

    values.forEach((val) => {
      balances.set(val.key, val.value);
      lastKey = val.key;
    });

    return {
      balances,
      next_cursor: lastKey,
    };
  }

  public async tokenBalance(
    contractAddress: BlockchainAddress,
    owner: BlockchainAddress
  ): Promise<BN> {
    if (this.balanceAvlTree === undefined) {
      await this.basicState(contractAddress);
    }
    const value = await this.balanceAvlTree?.get(owner);
    if (value === undefined) {
      throw new Error("No balance for user: " + owner.asString());
    }
    return value;
  }
}
