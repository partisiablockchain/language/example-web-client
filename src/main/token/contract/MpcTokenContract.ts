/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "@partisiablockchain/abi-client";
import BN from "bn.js";
import { transfer } from "../../abi/MpcToken";
import { TokenContract } from "../../shared/TokenContract";
import {
  BlockchainTransactionClient,
  ChainControllerApi,
  SentTransaction,
} from "@partisiablockchain/blockchain-api-transaction-client";

export const MPC_TOKEN_CONTRACT_ADDRESS: BlockchainAddress = BlockchainAddress.fromString(
  "01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881"
);

/*eslint @typescript-eslint/no-unused-vars: ["error", { "argsIgnorePattern": "^unused" }]*/
export class MpcTokenContract implements TokenContract {
  private readonly transactionClient: BlockchainTransactionClient | undefined;
  private readonly client: ChainControllerApi;

  constructor(
    shardedClient: ChainControllerApi,
    transactionClient: BlockchainTransactionClient | undefined
  ) {
    this.transactionClient = transactionClient;
    this.client = shardedClient;
  }

  /**
   * Build and send transfer transaction.
   * @param contractAddress address of the contract
   * @param to receiver of tokens
   * @param amount number of tokens to send
   */
  public transfer(
    contractAddress: BlockchainAddress,
    to: BlockchainAddress,
    amount: BN
  ): Promise<SentTransaction> {
    if (this.transactionClient === undefined) {
      throw new Error("No account logged in");
    }
    if (contractAddress.asString() !== MPC_TOKEN_CONTRACT_ADDRESS.asString()) {
      throw new Error("MpcTokenContract is only supported with the actual MPC_TOKEN_CONTRACT");
    }

    // First build the RPC buffer that is the payload of the transaction.
    const rpc = transfer(to, amount);
    // Then send the payload via the transaction API.
    // We are sending the transaction to the configured address of the token address, and use the
    // GasCost utility to estimate how much the transaction costs.
    return this.transactionClient.signAndSend({ address: contractAddress.asString(), rpc }, 16_250);
  }
}
