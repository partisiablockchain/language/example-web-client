/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";
import { BlockchainAddress } from "@partisiablockchain/abi-client";
import { SentTransaction } from "@partisiablockchain/blockchain-api-transaction-client";

export interface TokenContractBasicState {
  name: string;
  decimals: number;
  symbol: string;
  owner: BlockchainAddress;
  totalSupply: BN;
}

export interface TokenBalancesResult {
  balances: Map<BlockchainAddress, BN>;
  next_cursor: BlockchainAddress | undefined;
}

/**
 * Unified API for token contracts.
 *
 * This minimal implementation only allows for transferring tokens to a single address.
 *
 * The implementation uses the TransactionApi to send transactions, and ABI for the contract to be
 * able to build the RPC for the transfer transaction.
 */
export interface TokenContract {
  /**
   * Build transfer transaction RPC.
   *
   * @param to receiver of tokens
   * @param amount number of tokens to send
   */
  readonly transfer: (
    contractAddress: BlockchainAddress,
    to: BlockchainAddress,
    amount: BN
  ) => Promise<SentTransaction>;

  /**
   * Determines the basic state of the contract.
   */
  readonly basicState?: (contractAddress: BlockchainAddress) => Promise<TokenContractBasicState>;

  /**
   * Fetch a specific balance
   */
  readonly tokenBalance?: (
    contractAddress: BlockchainAddress,
    owner: BlockchainAddress
  ) => Promise<BN>;

  /**
   * Iterator for fetching token balances.
   */
  readonly tokenBalances?: (
    contractAddress: BlockchainAddress,
    cursor: BlockchainAddress | undefined
  ) => Promise<TokenBalancesResult>;
}
