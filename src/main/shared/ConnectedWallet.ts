/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BlockchainTransactionClient,
  SentTransaction,
  Transaction,
} from "@partisiablockchain/blockchain-api-transaction-client";

/**
 * Unified interface for connected MPC wallets.
 *
 * These wallets are capable of reporting their address and can sign and send
 * a transaction.
 */
export interface ConnectedWallet {
  /**
   * The address that transactions will be sent from.
   */
  readonly address: string;
  /**
   * Method to sign and send a transaction to the blockchain.
   */
  readonly signAndSendTransaction: (
    client: BlockchainTransactionClient,
    payload: Transaction,
    cost?: string | number
  ) => Promise<SentTransaction>;
}
