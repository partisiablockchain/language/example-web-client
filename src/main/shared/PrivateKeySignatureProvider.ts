/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// eslint-disable-next-line import/no-extraneous-dependencies
import {
  SenderAuthentication,
  Signature,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { ec } from "elliptic";
import { CryptoUtils } from "../client/CryptoUtils";
import { BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";

/**
 * Initializes a ConnectedWallet by inputting the private key directly.
 */
export const connectPrivateKey = async (
  sender: string,
  keyPair: ec.KeyPair
): Promise<SenderAuthentication> => {
  return {
    getAddress: () => sender,
    sign: (transactionPayload: Buffer, chainId: string): Promise<Signature> => {
      const hash = CryptoUtils.hashBuffers([
        transactionPayload,
        BigEndianByteOutput.serialize((out) => out.writeString(chainId)),
      ]);
      return Promise.resolve(CryptoUtils.signatureToBuffer(keyPair.sign(hash)).toString("hex"));
    },
  };
};
