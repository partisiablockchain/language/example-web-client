/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { PbcLedgerClient } from "../pbc-ledger-client/PbcLedgerClient";
import { listen } from "@ledgerhq/logs";
import {
  SenderAuthentication,
  Signature,
} from "@partisiablockchain/blockchain-api-transaction-client";
import { CryptoUtils } from "../client/CryptoUtils";
import TransportWebUSB from "@ledgerhq/hw-transport-webusb";

const DEFAULT_KEYPATH = "44'/3757'/0'/0/0";

/**
 * Initializes a ConnectedWallet by connecting to a Ledger Hardware Wallet.
 *
 * Both the initial connection and sending of a transaction will require the
 * user to interact with their Ledger device.
 *
 * Does not take any arguments as everything is automatically determined from the
 * environment.
 */
export const connectLedger = async (): Promise<SenderAuthentication> => {
  const ledgerTransport = await TransportWebUSB.create();

  //listen to the events which are sent by the Ledger packages in order to debug the app
  // eslint-disable-next-line no-console
  listen((log) => console.log(log));

  const ledgerClient = new PbcLedgerClient(ledgerTransport);
  const senderAddress = await ledgerClient.getAddress(DEFAULT_KEYPATH);

  return {
    getAddress: () => senderAddress,
    sign: async (transactionPayload: Buffer, chainId: string): Promise<Signature> => {
      return Promise.resolve(
        CryptoUtils.signatureToBuffer(
          await ledgerClient.signTransaction(DEFAULT_KEYPATH, transactionPayload, chainId)
        ).toString("hex")
      );
    },
  };
};

/**
 * Shows the address on the Ledger.
 *
 * Does not take any arguments as everything is automatically determined from the
 * environment.
 */
export const validateLedgerConnection = async (): Promise<void> => {
  const ledgerTransport = await TransportWebUSB.create();
  const ledgerClient = new PbcLedgerClient(ledgerTransport);
  await ledgerClient.getAddress(DEFAULT_KEYPATH, true);
};
